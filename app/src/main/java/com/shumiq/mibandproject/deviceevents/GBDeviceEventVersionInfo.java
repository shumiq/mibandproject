package com.shumiq.mibandproject.deviceevents;

import com.shumiq.mibandproject.GBApplication;
import com.shumiq.mibandproject.R;

public class GBDeviceEventVersionInfo extends GBDeviceEvent {
    public String fwVersion = GBApplication.getContext().getString(R.string.n_a);
    public String hwVersion = GBApplication.getContext().getString(R.string.n_a);
}
