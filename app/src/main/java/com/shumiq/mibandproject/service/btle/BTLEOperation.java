package com.shumiq.mibandproject.service.btle;

import java.io.IOException;

public interface BTLEOperation {
    void perform() throws IOException;
}
