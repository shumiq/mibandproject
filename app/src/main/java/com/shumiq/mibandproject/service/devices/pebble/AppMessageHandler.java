package com.shumiq.mibandproject.service.devices.pebble;


import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import com.shumiq.mibandproject.deviceevents.GBDeviceEvent;
import com.shumiq.mibandproject.deviceevents.GBDeviceEventSendBytes;
import com.shumiq.mibandproject.impl.GBDevice;
import com.shumiq.mibandproject.model.WeatherSpec;
import com.shumiq.mibandproject.util.FileUtils;

class AppMessageHandler {
    final PebbleProtocol mPebbleProtocol;
    final UUID mUUID;
    protected Map<String, Integer> messageKeys;

    AppMessageHandler(UUID uuid, PebbleProtocol pebbleProtocol) {
        mUUID = uuid;
        mPebbleProtocol = pebbleProtocol;
    }

    public boolean isEnabled() {
        return true;
    }

    public UUID getUUID() {
        return mUUID;
    }

    public GBDeviceEvent[] handleMessage(ArrayList<Pair<Integer, Object>> pairs) {
        // Just ACK
        GBDeviceEventSendBytes sendBytesAck = new GBDeviceEventSendBytes();
        sendBytesAck.encodedBytes = mPebbleProtocol.encodeApplicationMessageAck(mUUID, mPebbleProtocol.last_id);
        return new GBDeviceEvent[]{sendBytesAck};
    }

    public GBDeviceEvent[] onAppStart() {
        return null;
    }

    public byte[] encodeUpdateWeather(WeatherSpec weatherSpec) {
        return null;
    }

    protected GBDevice getDevice() {
        return mPebbleProtocol.getDevice();
    }

    protected JSONObject getAppKeys() throws IOException, JSONException {
        File destDir = new File(FileUtils.getExternalFilesDir() + "/pbw-cache");
        File configurationFile = new File(destDir, mUUID.toString() + ".json");
        if (configurationFile.exists()) {
            String jsonstring = FileUtils.getStringFromFile(configurationFile);
            JSONObject json = new JSONObject(jsonstring);
            return json.getJSONObject("appKeys");
        }
        throw new IOException();
    }
}