package com.shumiq.mibandproject.model;

import java.util.Date;

public interface ValidByDate {
    Date getValidFromUTC();
    Date getValidToUTC();
}
