package com.shumiq.mibandproject.activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shumiq.mibandproject.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by IQ on 21/2/2560.
 */

public class ChatMain extends GBActivity {

    private ListView mMessageListView;
    private EditText mMessageEditText;
    private ImageButton mSendButton;
    private MessageAdapter mMessageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mMessageListView = (ListView) findViewById(R.id.messageListView);
        mMessageEditText = (EditText) findViewById(R.id.messageEditText);
        mSendButton = (ImageButton) findViewById(R.id.sendButton);

        // Initialize message ListView and its adapter
        List<ChatMessage> chatMessages = new ArrayList<>();
        mMessageAdapter = new MessageAdapter(this, R.layout.item_chat_message, chatMessages);
        mMessageListView.setAdapter(mMessageAdapter);

        // Enable Send button when there's text to send
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        //mMessageEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(DEFAULT_MSG_LENGTH_LIMIT)});

        // Send button sends a message and clears the EditText
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: Send messages on click
                ChatMessage chatMessage = new ChatMessage(mMessageEditText.getText().toString(), DashBoard.user.getEmail());
                try {
                    FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/chat/").push().setValue(chatMessage);
                    if(Math.random()>0.5)
                        FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/chat/").push().setValue(new ChatMessage("ขออภัย ระบบนี้ยังไม่พร้อมใช้งาน", "แพทย์"));
                } catch (Exception e) {}
                // Clear input box
                mMessageEditText.setText("");
            }
        });

        FirebaseDatabase.getInstance().getReference("user/"+DashBoard.userUID+"/chat/").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                    ChatMessage chatMessage = dataSnapshot.getValue(ChatMessage.class);
                    mMessageAdapter.add(chatMessage);
                } catch (Exception e){}
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}

class ChatMessage {

    private String text;
    private String name;

    public ChatMessage() {
    }

    public ChatMessage(String text, String name) {
        this.text = text;
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class MessageAdapter extends ArrayAdapter<ChatMessage> {
    public MessageAdapter(Context context, int resource, List<ChatMessage> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.item_chat_message, parent, false);
        }

        TextView messageTextView = (TextView) convertView.findViewById(R.id.messageTextView);
        TextView authorTextView = (TextView) convertView.findViewById(R.id.nameTextView);
        RelativeLayout chatBubble = (RelativeLayout) convertView.findViewById(R.id.chatBubble);
        RelativeLayout chatLayout = (RelativeLayout) convertView.findViewById(R.id.chatLayout);

        ChatMessage message = getItem(position);


        messageTextView.setVisibility(View.VISIBLE);
        messageTextView.setText(message.getText());

        authorTextView.setText(message.getName());

        //Go to Right if Doctor
        if(!message.getName().equals(DashBoard.user.getEmail())){
            chatBubble.setBackgroundResource(R.color.chat_bubble_in);
            messageTextView.setTextColor(Color.WHITE);
            chatLayout.setGravity(Gravity.RIGHT);
            authorTextView.setGravity(Gravity.RIGHT);
        }
        else{
            chatBubble.setBackgroundResource(R.color.chat_bubble_out);
            messageTextView.setTextColor(Color.BLACK);
            chatLayout.setGravity(Gravity.LEFT);
            if(DashBoard.userName!="")authorTextView.setText(DashBoard.userName);
            authorTextView.setGravity(Gravity.LEFT);
        }

        return convertView;
    }
}
